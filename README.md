<div align="center">

![VERY Temporary Logo! Damn I Need To Hire An Actual Artist...](project-solver/assets/splash-screen-ingame.png)

[![](https://img.shields.io/badge/matrix-chat%20here!-brightgreen)](https://matrix.to/#/#competitive-godot-fps:matrix.org) [![](https://img.shields.io/badge/version-0.8.0--alpha-blue)](https://codeberg.org/solver-orgz/project-solver)

This project is a First Person Shooter that aims to be the most versatile and fun tactical shooter there is! Made with Godot Engine, it follows concepts based on the proposal at [`documents/PROPOSAL.md`](documents/PROPOSAL.md). Developers, artists, playtesters, and even beginners are more than welcome to contribute!

Created by Tengku Izdihar.

[Create your first proposal/issue here!](https://codeberg.org/solver-orgz/project-solver/issues/new/choose)

</div>

# Table Of Contents
- [Table Of Contents](#table-of-contents)
- [Development Status](#development-status)
- [Screenshot](#screenshot)
- [How to Contribute](#how-to-contribute)
- [Installing and Building The Game](#installing-and-building-the-game)
- [Roadmap](#roadmap)
- [Technical FAQ](#technical-faq)

# Development Status

![Dev Badge](https://img.shields.io/badge/Development%20-Active-brightgreen)

For any question, you can contact the project owner on matrix https://matrix.to/#/#competitive-godot-fps:matrix.org. Before tinkering with the project, please note that this project uses **GIT-LFS**. Make sure your git has it.

# Screenshot

<p align="center">
<img src="documents/assets/gameplay.png" width="45%"></img>
<img src="documents/assets/gameplay_main_menu_select_level.png" width="45%"></img>
<img src="documents/assets/gameplay_pause_menu_option.png" width="45%"></img>
<img src="documents/assets/gameplay_sniper_show_off.png" width="45%"></img>
</p>

Screenshot might be inaccurate, it's still in development after all.

# How to Contribute

Please read [documents/CONTRIBUTING.md](documents/CONTRIBUTING.md) for further assistance.

# Installing and Building The Game

Head over to [compogodot/REAMDE.md](project-solver/README.md) for more information. The game's source code is located under `project-solver/`.

# Roadmap

Head over to [documents/ROADMAP.md](documents/ROADMAP.md) for further detail.

# Technical FAQ

- **I'm on NixOS and I can't run any game that's exported from Godot!**

  Run the game by going into root directory where the game is, and then use `steam-run <location of the game file>`. For example, `steam-run ./game.x86_64`.

  This of course happens because of how NixOS is structured and it seems like Godot does rely on some library such as `/lib64/ld-linux-x86-64.so.2` (in which every single 64 bit linux distribution should have).
