# Roadmap

This document is all about the big picture. Right now, the project does have milestones and an end game for the lifetime of the project. The endgame itself is not the end of the project, but rather the start of a new era where every single feature is implemented, at least for a prototype. The project is called "Project Solver". "Project Solver" is a game that would evolve using the effort of the developer and or community, until the "perfect" or at least "usable" game could exist. Below is the list of games and their features.

## **Project Solver: Training**

The first game in Project Solver that's all about aiming and training. It features customizations and offline capabilities for those who wants it. The game itself would focus on self improvement in the aiming department and will feature source-like movement as its main locomotion system. The features intended to be in the game is listed below.

- [x] Variety of target to shot at
- [x] Different weapons for different needs
  - [x] Guns
    - [x] General Concepts
      - [x] Spray Pattern
      - [x] Recoil
      - [x] Inaccuracy (e.g. based on movement)
      - [x] Bullet Penetration
    - [x] Automatic Weapon (e.g. AK-47)
    - [x] Semi-Automatic Weapon (e.g. M9 Pistol)
    - [x] Scoped Semi-Automatic Weapon (e.g. Sniper Rifle)
  - [x] Utilities
    - [x] General Concepts
      - [x] Right Click vs Left Click Throw
      - [x] Timed Detonation
    - [x] Frag Grenade
    - [x] Smoke Grenade
    - [x] Flash Grenade
- [x] Training Methods
  - [x] Spray Practice
  - [x] Moving Target Practice
  - [x] Time Attack Practice (shoot target through corridor as fast as possible)
- [x] Movement system that's inspired by the Counter Strike series
  - [x] Crouch Jumping
  - [x] Bunny Hopping
  - [x] Air Strafing
  - [x] Counter Strafing
- [x] Sound Design
  - [x] footsteps
    - [x] static (only one kind of surface)
- [x] Light weight experience that's not only targeting the toaster, but also your beefed up $500,000, NASA made, super computer

## **Project Solver: 1v1** (CURRENT PHASE)

The second game in Project Solver that's all about 1v1 PVP. This game will include every single feature of The Aim Solver but now with a lot more variety of weapons and game modes. In multiplayer, the server will have the highest authority on what can or can't be done. This attributes will contribute to make the game much safer from cheaters and the like. The features intended to be in the game will be,

- [ ] Multiplayer that features 2 players fighting against each other.
- [ ] Replay features that's modular and can be copied by the user for save keeping purposes.
- [ ] Matchmaking based on the reputation and skill of the player.
- [ ] Statistics and data that the user can use to know which skill they need to improve upon.
- [ ] Guns
  - [ ] Shotgun
- [ ] Utilities
  - [ ] Incendiary Grenade
- [ ] Sound Design
  - [ ] footsteps
    - [ ] adaptive (many kinds of surfaces)
  - [ ] Etc.
- [ ] Animation
  - [ ] Better gun animation
  - [ ] Player is now animated for all different actions
- [ ] Easy to use UI customization that also includes an export and import function, for portability.
- [ ] Importing mouse sensitivity from other games
  - [ ] CSGO
  - [ ] Valorant
  - [ ] Call Of Duty

## **Project Solver: 5v5**

The endgame of Project Solver that's all about 5v5 PVP. This game will also include every single feature of its predecessor and now with a working competitive mode that will clash two teams made out of 5 players doing their best to win.

- [ ] A better more robust matchmaking that's suited for a 10 player per match.
- [ ] A nicer and universal spectating system so other could use their client to do so.
- [ ] Delay based spectating for competitive matches.
- [ ] A transparent, fair, and community building ranking system.