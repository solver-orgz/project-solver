extends GPUParticles3D

var duration = 2.5

func _ready() -> void:
	emitting = true
	$FogVolume.material.density = -8.0

	var tween = get_tree().create_tween()
	tween.tween_interval(1.5)
	
	# will be parallel
	tween.tween_property($FogVolume.material, "density", 0.0, self.duration).set_ease(Tween.EASE_OUT)
	tween.parallel().tween_property($FogVolume, "size", Vector3(), self.duration).set_ease(Tween.EASE_OUT)
	
	tween.tween_callback(func(): queue_free())
