extends Node3D

var custom_length = 0.1


func _ready():
	$FogVolume.material.density = -8


func _physics_process(_delta):
	var ratio = $KillTimer.time_left / $KillTimer.wait_time

	$FogVolume.size.x = lerp(0.0, 1.0, ratio)
	$FogVolume.size.z = lerp(0.0, 1.0, ratio)


func set_length(length: float) -> void:
	$FogVolume.size.y = length
	$DebugMesh.mesh.height = length


func _on_timer_timeout():
	queue_free()
