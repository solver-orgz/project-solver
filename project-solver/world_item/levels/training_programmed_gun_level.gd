extends Node3D

const METRONOME_COUNT_DOWN = 3
const CENTER_TEXT_BEFORE_START = "PRESS FIRE TO START"

var player_scene = preload("res://player/player.tscn")
var aiming_weapon_scene = preload("res://world_item/guns/infinite_program_triggered_pistol.tscn")

var player
var aiming_weapon
var player_pivot_node
var metronome

var _player_starts_training = false

func _ready():
	# Setup Environment
	Util.handle_err(Score.connect("rst_finished", Callable(self, "_on_Score_rst_finished")))
	State.set_state("player_center_text", CENTER_TEXT_BEFORE_START)

	# Resurrect Them
	metronome = $Metronome
	var universal_spawn_point = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL).pop_front()
	player = player_scene.instantiate()

	# Add To World3D
	# Consequently, will be handled by handle_added_player automagically in _on_tree_node_added
	Util.add_to_world(player)
	player.global_transform = universal_spawn_point.global_transform

	# give player gun with infinite ammo and no recoil
	aiming_weapon = aiming_weapon_scene.instantiate()
	Util.add_to_world(aiming_weapon)
	player.replace_weapon_and_free_existing(aiming_weapon)
	player._switch_weapon_routine(aiming_weapon.weapon_slot)

	player_pivot_node = player.pivot  # should I wait for player to be ready?

	# disable player movement input (aiming is still enabled) and processing
	player.disable_movement = true

	# disable player throwing guns (using "g")
	player.disable_weapon_drop = true

	# TODO: also react to changes in history
	metronome.bpm = Score.training_history[Score.RST_CURRENT_METRONOME_BPM]

	Score.rst_finished.connect(self._on_self_rst_finished)
	Score.training_history_updated.connect(self._on_Score_training_history_updated)


func _physics_process(_delta):
	var is_paused = State.get_state("player_paused")
	if not is_paused and not _player_starts_training and LLInput.is_action_pressed("player_shoot_primary"):
		self.start_training()


func _on_Score_rst_finished(_elapsed_seconds, _max_hit, _shot_count_hit, _shot_count_not_hit) -> void:
	self.finished_training()


func _on_Metronome_ticked(_bpm: float, tick_count: int) -> void:
	set_center_text_from_metronome(tick_count)

	# Countdown tick shouldn't trigger the weapon
	if tick_count <= METRONOME_COUNT_DOWN:
		return

	aiming_weapon.trigger_on(0.0)
	aiming_weapon.first_activate(player_pivot_node)
	aiming_weapon.trigger_off()


func _on_Score_training_history_updated(history_dict: Dictionary) -> void:
	metronome.bpm = history_dict[Score.RST_CURRENT_METRONOME_BPM]


func start_training() -> void:
	_player_starts_training = true
	metronome.start_metronome()


func finished_training() -> void:
	_player_starts_training = false
	metronome.stop_metronome()
	self.set_center_text_from_metronome(0)


func set_center_text_from_metronome(metronome_tick_count: int) -> void:
	if metronome_tick_count > METRONOME_COUNT_DOWN:
		State.set_state("player_center_text", "")

	if metronome_tick_count <= METRONOME_COUNT_DOWN and metronome_tick_count > 0:
		State.set_state("player_center_text", "%d" % metronome_tick_count)

	if metronome_tick_count <= 0:
		State.set_state("player_center_text", CENTER_TEXT_BEFORE_START)


func _on_self_rst_finished(elapsed_seconds: float, max_hit_before_timer: int, shot_count_hit: int, shot_count_not_hit: int) -> void:
	var accuracy = 0.0
	if (shot_count_hit + shot_count_not_hit) > 0:
		accuracy = float(shot_count_hit) / float(shot_count_hit + shot_count_not_hit)

	Score.rst_metronome_add_history_and_save(Time.get_datetime_string_from_system(true, true), elapsed_seconds, accuracy, max_hit_before_timer, metronome.bpm)
	Score.reset_rst_metronome_state()
