@tool
extends SubViewportContainer

@export var world_camera_path: NodePath

var my_camera: Camera3D
var world_camera: Camera3D
@onready var gun_viewport = $GunViewport


func _get_configuration_warnings() -> PackedStringArray:
	var cam_node = get_node(world_camera_path)
	if not cam_node and not (cam_node is Camera3D):
		return ["World3D Camera3D Path3D need to be set!"]
	return []


# TODO: this script what makes the game RENDERS TWICE WHAT THE FUCK
func _ready() -> void:
	if not Engine.is_editor_hint():
		my_camera = $GunViewport/Camera3D
		world_camera = get_node(world_camera_path)


func _process(_delta: float) -> void:
	if not Engine.is_editor_hint():
		my_camera.global_transform = world_camera.global_transform
