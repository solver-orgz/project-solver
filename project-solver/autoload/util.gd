extends Node

# Clamping the length of a single vector3
func clamp_vector3(vec: Vector3, max_length: float) -> Vector3:
	return Vector3(clamp(vec.x, -max_length, max_length), clamp(vec.y, -max_length, max_length), clamp(vec.z, -max_length, max_length))


func clamp_vector3_y_axis(vec: Vector3, max_length: float) -> Vector3:
	return Vector3(clamp(vec.x, -max_length, max_length), vec.y, clamp(vec.z, -max_length, max_length))


func init_interface_node(target: Node, interface: Node) -> Node:
	target.add_child(interface)
	return interface


func add_to_world(node: Node) -> void:
	var level_groups = get_tree().get_nodes_in_group(Global.GROUP_LEVEL)
	if level_groups.size() > 0:
		level_groups.front().add_child(node)
		return

	for i in get_tree().root.get_children():
		if i is Node3D:
			i.add_child(node)


func array_get(array: Array, index: int, default = null):
	if index < 0:
		printerr("Getting array with index below zero {}", get_stack())
		return default
	elif index > array.size() - 1:
		return default
	else:
		return array[index]


func handle_err(error_code: int) -> void:
	if error_code > 0:
		printerr("ERROR CODE: ", error_code, "\n", get_stack())


func change_level(scene_path: String, game_mode: int = Global.GAME_MODE.DEFAULT) -> void:
	State.reset_game()
	State.set_state("game_mode", game_mode)
	handle_err(get_tree().change_scene_to_file(scene_path))


func get_groups_with_prefix(node: Node, prefix: String) -> Array:
	var result = []
	for g in node.get_groups():
		if g.begins_with(prefix):
			result.append(g)
	return result


func free_in_group_when_exceeding(group: String, limit: int):
	var nodes = get_tree().get_nodes_in_group(group)
	var freed_size = max(nodes.size() - limit, 0)

	for i in range(0, freed_size):
		var node = nodes[i]
		if !node.is_queued_for_deletion():
			node.queue_free()


func directory_file_names(directory_path, extension = "") -> Array:
	var file_names = []
	var dir = DirAccess.open(directory_path)

	if dir:
		dir.list_dir_begin()  # TODOGODOT4 fill missing arguments https://github.com/godotengine/godot/pull/40547
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir():
				if extension.is_empty() or (!extension.is_empty() and file_name.ends_with("." + extension)):
					file_names.append(file_name)
			file_name = dir.get_next()
	else:
		printerr("An error occurred when trying to access the path.")

	return file_names


func is_in_either_group(node: Node, group_name: Array):
	var check = false

	for g in group_name:
		check = check or node.is_in_group(g)

	return check


func is_player(node: Node) -> bool:
	return "is_player" in node


func fov_from_zoom_mode(value: int):
	match value:
		Global.WEAPON_ZOOM_MODE.DEFAULT:
			return Global.FOV_DEFAULT
		Global.WEAPON_ZOOM_MODE.SNIPER_ZOOMED_1:
			return Global.FOV_SNIPER_ZOOMED_1
		Global.WEAPON_ZOOM_MODE.SNIPER_ZOOMED_2:
			return Global.FOV_SNIPER_ZOOMED_2
		_:
			return Global.FOV_DEFAULT


# return an array of cartessian [x,y] coordinates
# inspired by https://stackoverflow.com/a/50746409/17531052
func randomized_point_in_circle(radius: float) -> Array:
	randomize()
	var theta = randf() * 2 * PI
	var randomized_radius = radius * sqrt(randf())

	return [
		randomized_radius * cos(theta),
		randomized_radius * sin(theta)
	]


func string_is_prefixed_by_list(key: String, prefix_list: Array[String]) -> bool:
	for prefix in prefix_list :
		if key.begins_with(prefix):
			return true

	return false
