extends Node

const SIGNAL_PREFIX = "state_"

# list of state prefixes that will be exluded when reset_state() is called
# for example, when changing levels.
const STATE_PREFIX_EXCLUDE_RESET: Array[String] = [
	"multiplayer_"
]

const INITIAL_STATE = {
	"debug_player_velocity": 0.0,
	"debug_misc": "NOT USED",
	"debug_ammo": "NOT USED",

	# Game States
	"game_mode": Global.GAME_MODE.DEFAULT,
	"game_mode_state": {
		"state": Global.DEFAULT_GAME_MODE_STATE.LOBBY,
		"gmd" : {
			"state_starting_end_unix": 0, # unix time marking the end of the starting phase
			"state_ongoing_end_unix": 0, # unix time marking the end of the starting phase
			"state_ending_end_unix": 0, # unix time marking the end of the starting phase
		}
	}, # custom structs used by each game mode, but for now defaulted to Global.GAME_MODE.DEFAULT

	# Should be "true" whenever the player pause the game
	# This should happen in situation such as; opening the escape menu
	"player_paused": false,
	"player_buy_menu_open": false,

	# The physics frame which the player reloaded
	"player_reloaded": 0,

	# Use Global.WEAPON_ZOOM_MODE.DEFAULT for default value
	# example: { peer_id: Global.WEAPON_ZOOM_MODE.DEFAULT }
	"player_zoom_mode": {},

	# The current velocity length of the player
	# Unlike debug_player_velocity, value is real and not stepified
	"player_velocity_length": 0.0,

	# The current weapon's status
	"player_weapon_name": "Current Weapon",
	"player_weapon_current_ammo": 0,
	"player_weapon_total_ammo": 0,

	# The current player's health status
	"player_health": 0.0,
	"player_armor": 0.0,

	# What shows up in player's GUI, very distracting, use cautiously
	"player_center_text": "",

	# Shooting Target Configurations
	# WARNING: The default movement mode is using Global.SHOOTING_TARGET_MOVEMENT_MODE.STATIC
	# WARNING: The default size is the same as the default scale (1)
	"shooting_target_movement_mode": 0,
	"shooting_target_size": 1.0,

	# Based checked the enum supplied in Global.AMMO_INFINITE_MODE
	"ammo_infinite_mode": 0,

	"multiplayer_lobby_info": {
		"level_path": "res://world_item/levels/multiplayer_test_level.tscn"
	},

	# Will be used to present the player summary, mostly used in lobby and quick menu (tab menu when in-game)
	# built using build_multiplayer_lobby_player_info()
	"multiplayer_lobby_player_info": {},
	"multiplayer_lobby_password": "",
	"multiplayer_lobby_is_entered": false,

	# {
	# 	<peer id>: {
	# 		"player": <player instance>
	# 	}
	# }
	"multiplayer_player_info": {},

	# used for spawning in players in game_mode.gd
	# {
	#     "spawn_global_transform": Transform3D(),
	#     "peer_id": int,
	#     "team_enum": Global.TEAM
	# }
	"multiplayer_spawn_queue": [],

	# dictionary of items that need to be managed when their state changes (especially when being equipped by a player)
	# the format is ID => ITEM INSTANCE, with ID typed string and ITEM typed NODE
	#
	# WARNING: the value of a key might be null after queue_free().
	"multiplayer_item_alive": {},

	"multiplayer_delay": 0.0,

	"multiplayer_delay_random": 0.0,
}

# To add state, make sure that the key is STRING and the value is anything
# besides null. Also, please use duplicate, because dict is copy by reference
# not value.
@onready var _state = INITIAL_STATE.duplicate(true)


func _ready():
	# Add a signal based checked the keys checked _state. This way, any entity can subscribe
	# to a state everytime it changes.
	for s in INITIAL_STATE.keys():
		add_user_signal(SIGNAL_PREFIX + s, [typeof(_state[s])])


# Can only change state that's already inside of _state
func set_state(state_name: String, value, no_emit_signal = false):
	var signal_name = SIGNAL_PREFIX + state_name
	_state[state_name] = value

	if not no_emit_signal and has_signal(signal_name) and typeof(value) == typeof(_state[state_name]):
		emit_signal(SIGNAL_PREFIX + state_name, value)
	if not (has_signal(signal_name) and typeof(value) == typeof(_state[state_name])):
		print_stack()
		push_error("Signal named: " + signal_name + " doesn't exist or value is not compatible with currently state value!")


func get_state(state_name: String):
	if _state.has(state_name):
		return _state[state_name]
	else:
		print_stack()
		push_error("State named: " + state_name + " doesn't exist!")
		return null


func reset_excluded_state():
	for k in INITIAL_STATE.keys():
		# reset any state with certain prefixes
		if Util.string_is_prefixed_by_list(k, STATE_PREFIX_EXCLUDE_RESET):
			set_state(k, INITIAL_STATE[k])


# Reset the state through set_state so the signals and other routines could be
# executed and deployed by the existing scenes.
func reset_state():
	for k in INITIAL_STATE.keys():
		# don't reset any state with certain prefixes
		if Util.string_is_prefixed_by_list(k, STATE_PREFIX_EXCLUDE_RESET):
			continue

		set_state(k, INITIAL_STATE[k])


# Reset the state and other autoloads that have in game knowledge that we
# need to reset.
func reset_game():
	reset_state()
	Score.reset_state()


func build_multiplayer_lobby_player_info(nametag: String, kill: int, death: int, assist: int, team_enum: Global.TEAM) -> Dictionary:
	return {
		"nametag": nametag,
		"kill": kill,
		"death": death,
		"assist": assist,
		"team_enum": team_enum,
	}


func create_delay_awaitable() -> void:
	var delay = State.get_state("multiplayer_delay") + randf_range(0.0, State.get_state("multiplayer_delay_random"))
	await get_tree().create_timer(delay).timeout


func get_zoom_level(peer_id: int) -> Global.WEAPON_ZOOM_MODE:
	var state = self.get_state("player_zoom_mode")

	if not (peer_id in state):
		self.set_zoom_level(peer_id, Global.WEAPON_ZOOM_MODE.DEFAULT, true)

	return self.get_state("player_zoom_mode")[peer_id]


func set_zoom_level(peer_id: int, zoom_level: Global.WEAPON_ZOOM_MODE, no_emit_signal = false):
	var current = self.get_state("player_zoom_mode").duplicate(true)
	current[peer_id] = zoom_level

	self.set_state("player_zoom_mode", current, no_emit_signal)


func build_multiplayer_lobby_info(level_path: String) -> Dictionary:
	return {
		"level_path": level_path
	}