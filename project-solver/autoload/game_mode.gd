extends Node

var player_scene = preload("res://player/player.tscn")
var player_group_names = [Global.SPAWN_TYPE_GROUP_UNIVERSAL, Global.SPAWN_TYPE_GROUP_TEAM_1, Global.SPAWN_TYPE_GROUP_TEAM_2]

# array of dictionary of player state data
# for clients I think filling this with other player's data is unnecessary
# the structure are as followed
# 	{
# 		peer_id: DEFAULT_PLAYER_SNAPSHOT
# 	}
var player_snapshot_timeline = []

const DEFAULT_TIMELINE_SIZE = 75 * 10  # 75 physics frame per second with 10 seconds of rollback
const DEFAULT_PHYSICS_SYNC_GAP = 10  # will sync physics object to player every 10 frames instead of every frame to avoid jitter

const DEFAULT_PLAYER_SNAPSHOT = {  # TODO: also record animation state so we can rollback the position of things too
	"global_transform": Transform3D(),
	"velocity": Vector3(),
}

const WEAPON_REGISTRY = {
	"kf1": preload("res://world_item/guns/kf1.tscn"),
	"pm9": preload("res://world_item/guns/pm9.tscn"),
	"rail_gun": preload("res://world_item/guns/rail_gun.tscn"),
	"rf7": preload("res://world_item/guns/rf7.tscn"),
	"frag_grenade": preload("res://world_item/utility/frag_grenade.tscn"),
	"flash_grenade": preload("res://world_item/utility/flash_grenade.tscn"),
	"smoke_grenade": preload("res://world_item/utility/smoke_grenade.tscn"),
}

var INITIAL_DEFAULT_GAME_MODE_STATE = {
	"state": Global.DEFAULT_GAME_MODE_STATE.LOBBY,
}

# tag used in player_snapshot_timeline
const TAG_PLAYERS = "players"
const TAG_ITEMS = "items"

# only used by client for prediction, DON'T USE SERVER SIDE use Engine.get_physics_frames() instead
var multiplayer_latest_client_physics_tick_when_server_info_comes = 0
var multiplayer_latest_server_physics_tick = 0

func _ready():
	Util.handle_err(get_tree().connect("node_added", Callable(self, "_on_tree_node_added")))

	# init timeline data
	player_snapshot_timeline = []
	player_snapshot_timeline.resize(DEFAULT_TIMELINE_SIZE)
	player_snapshot_timeline.fill({
		TAG_PLAYERS: {},
		TAG_ITEMS: {},
	})


func _physics_process(delta):
	# all player must be processed before their info could get sent to client
	#
	# BUG: get_nodes_in_group might give players in random order, need to make
	#      util that would sort player and for loop it in deterministic way
	#      for replay purposes.
	for player in get_tree().get_nodes_in_group(Global.GROUP_PLAYER):
		if player.is_node_ready():
			player.physics_process(delta)

	if multiplayer.has_multiplayer_peer() and multiplayer.is_server():
		self.send_data_to_client() # don't await this let it flow

	if multiplayer.has_multiplayer_peer() and not multiplayer.is_server():
		var item_info = self.sync_item_info(true) # always update even for jitter when playing alone
		var client_physics_frame = Engine.get_physics_frames()

		for item_id in item_info.keys():
			self.populate_timeline_snapshot(client_physics_frame, TAG_ITEMS, item_id, item_info[item_id])

	var game_mode = State.get_state("game_mode")
	match game_mode:
		Global.GAME_MODE.DEFAULT:
			gmd_physics_process(delta)


func send_data_to_client() -> void:
	var server_physics_tick = Engine.get_physics_frames()
	var player_info = self.sync_player_info()
	var item_info = self.sync_item_info((server_physics_tick % 10) == 0)

	# we need to differentiate between player_info call and item_info call
	# if not, "above mtu" error might occur
	# https://codeberg.org/solver-orgz/project-solver/issues/324

	# TODO: maybe just create a different RPC for item_info
	#       this made the player jitters https://codeberg.org/solver-orgz/project-solver/issues/375
	# self.server_push_frame_data.rpc(server_physics_tick, {}, item_info)

	self.server_push_frame_data.rpc(server_physics_tick, player_info, item_info)


func sync_player_info() -> Array:
	var multiplayer_player_info = State.get_state("multiplayer_player_info")

	var players = []
	for peer_id in multiplayer_player_info.keys():
		var peer_info = multiplayer_player_info[peer_id]

		if peer_info != null:
			var player = multiplayer_player_info[peer_id]["player"]
			if player != null:
				players.append(player.get_multiplayer_pull_and_reset().duplicate(true))

	return players


func sync_item_info(can_update_for_jittery: bool) -> Dictionary:
	var item_info_map = {}
	var multiplayer_item_alive = State.get_state("multiplayer_item_alive")
	for k in multiplayer_item_alive.keys():
		var item = multiplayer_item_alive.get(k)
		if not item or not is_instance_valid(item):
			multiplayer_item_alive.erase(k)
			continue

		item_info_map[k] = {}

		if not is_instance_valid(item) or not item.is_inside_tree() or not item.is_node_ready():
			continue

		if can_update_for_jittery and item is RigidBody3D:
			item_info_map[k]["global_transform"] = item.global_transform
			item_info_map[k]["linear_velocity"] = item.linear_velocity
			item_info_map[k]["angular_velocity"] = item.angular_velocity

		if "player" in item and item.player:
			item_info_map[k]["player_id"] = item.player.owned_by_peer_id

		if "weapon_registry_key" in item and item.weapon_registry_key:
			item_info_map[k]["weapon_registry_key"] = item.weapon_registry_key

		if item is GenericWeapon and item.can_shoot() and not item.is_weapon_equipped():
			item_info_map[k]["current_ammo"] = item.current_ammo
			item_info_map[k]["current_total_ammo"] = item.current_total_ammo

	return item_info_map


###########################################################
# ENTITY SYNC STUFF
###########################################################

"""
TODO:	remove transform in the future or ignore it, for now client can manipulate their own position on their own will
		this is a temporary measure, it WILL ALLOW VERY EASY CLIENT SIDE TRANSFORM MANIPULATION aka. CHEATING.

client_trust_data structure:
{
	"transform": <transform 3D of player>
}
"""
@rpc("any_peer", "call_remote", "unreliable_ordered")
func client_push_player_data(server_physics_tick_that_client_uses: int, input: Vector3, actions: Array, parent_pivot_transform: Transform3D, pivot_transform: Transform3D, current_weapon_slot: int, client_trust_data: Dictionary) -> void:
	if not multiplayer.is_server():
		return

	var peer_id = multiplayer.get_remote_sender_id()
	var current_state = State.get_state("multiplayer_player_info")
	var player = current_state[peer_id]["player"]

	if player == null or not is_instance_valid(player):
		printerr("ERROR: Player Pushing Entity Data When Dead", get_stack())

	if player != null:
		var snapshot_data = DEFAULT_PLAYER_SNAPSHOT.duplicate(true)

		# TODO: trust client for now
		snapshot_data["global_transform"] = player.global_transform
		snapshot_data["velocity"] = player.velocity

		self.populate_timeline_snapshot(server_physics_tick_that_client_uses, TAG_PLAYERS, peer_id, snapshot_data)
		player.set_multiplayer_pull(server_physics_tick_that_client_uses, input, actions, parent_pivot_transform, pivot_transform, current_weapon_slot, client_trust_data, [])


"""
TODO: remove transform in the future or ignore it, for now client can manipulate their own position on their own will
	  this is a temporary measure, it WILL ALLOW VERY EASY CLIENT SIDE TRANSFORM MANIPULATION aka. CHEATING.

This function is called via rpc by the server, which will push a frame's data.

players: [
  [<peer_id>, <input>, <pivot_transform>, <client_trust_data>]
]

items: {
	"<item id>": {
		"global_transform": Transform3D(),
		"linear_velocity": Vector3(),
		"angular_velocity": Vector3(),
		"player_id": int
	}
}
"""
@rpc("authority", "call_remote", "unreliable_ordered")
func server_push_frame_data(server_physics_tick: int, players, items) -> void:
	var client_player_info = State.get_state("multiplayer_player_info")

	# build client_tick_to_compare before physics tick cache values are updated
	var client_tick_to_compare = 0
	if self.multiplayer_latest_server_physics_tick > 0:
		client_tick_to_compare = (server_physics_tick - self.multiplayer_latest_server_physics_tick) + self.multiplayer_latest_client_physics_tick_when_server_info_comes

	# State.set_state("debug_misc", "Δ server-client frames: " + str(client_tick_to_compare - self.multiplayer_latest_server_physics_tick))

	# update physics tick cache
	self.multiplayer_latest_server_physics_tick = server_physics_tick
	self.multiplayer_latest_client_physics_tick_when_server_info_comes = Engine.get_physics_frames()

	# apply server's player data to client
	self.server_player_data_apply(client_tick_to_compare, client_player_info, players)

	# apply server's item data to client
	self.server_item_data_apply(client_tick_to_compare, items)


func server_item_data_apply(client_tick_to_compare: int, items) -> void:
	var multiplayer_items = State.get_state("multiplayer_item_alive")
	var past_client_data = GameMode.get_timeline_snapshot(client_tick_to_compare, TAG_ITEMS)

	for k in items.keys():
		var local_item = multiplayer_items.get(k)
		var server_item_info = items.get(k)
		var item_past_data = past_client_data.get(k)

		if not local_item:
			self.add_weapon_to_player(-1, server_item_info["weapon_registry_key"], k)
			continue

		# TODO: what happened if:
		# - client is already dropping but server drops the gun 1 frame later? need reconciliation with server.
		# - server doesn't get the drop action but we dropped it? need reconsiliation.
		# - desync of bullet counts? probably need the timeline thingy to check

		# case in which it's in possession of the right owner
		if item_possesion_should_be_updated(item_past_data, server_item_info):
			var local_item_valid = local_item and "player" in local_item

			# give weapon to player
			if local_item_valid and item_is_owned_by_player(server_item_info):
				var peer_info = State.get_state("multiplayer_player_info")[server_item_info["player_id"]]
				if peer_info and peer_info["player"]:
					self.give_weapon_to_player_after_weapon_ready(peer_info["player"], local_item)

			# give weapon to the world
			if local_item_valid and not item_is_owned_by_player(server_item_info) and local_item.player:
				local_item.player.drop_weapon_external(local_item)

		# don't sync physics when it's in a player inventory
		var server_item_info_valid_transform = server_item_info and server_item_info.has("global_transform") and server_item_info.has("linear_velocity") and server_item_info.has("angular_velocity")

		if local_item and local_item is RigidBody3D and "player_id" not in server_item_info and server_item_info_valid_transform:
			local_item.global_transform = server_item_info["global_transform"]
			local_item.linear_velocity = server_item_info["linear_velocity"]
			local_item.angular_velocity = server_item_info["angular_velocity"]

		var past_data_valid = item_past_data and item_past_data.has("current_ammo") and item_past_data.has("current_total_ammo")
		var server_item_info_valid = server_item_info and server_item_info.has("current_ammo") and server_item_info.has("current_total_ammo")

		if local_item and local_item is GenericWeapon and past_data_valid and server_item_info_valid:
			if item_past_data["current_ammo"] != server_item_info["current_ammo"]:
				local_item.current_ammo = server_item_info["current_ammo"]
			if item_past_data["current_total_ammo"] != server_item_info["current_total_ammo"]:
				local_item.current_total_ammo = server_item_info["current_total_ammo"]


# WHY GOD WHY, WHY CANT YOU GIVE NULL CHECKER FOR GODOT LIKE TYPESCRIPT USING ?
func item_possesion_should_be_updated(item_a, item_b) -> bool:
	if not item_a or not item_b:
		return false

	var a_owned = item_is_owned_by_player(item_a)
	var b_owned = item_is_owned_by_player(item_b)

	if a_owned != b_owned:
		return true

	if get_item_player_id(item_a) != get_item_player_id(item_b):
		return true

	return false


func item_is_owned_by_player(item) -> bool:
	return item and item.has("player_id") and item.player_id >= 0


func get_item_player_id(item) -> int:
	if "player_id" in item:
		return item.player_id
	return 0


func server_player_data_apply(client_tick_to_compare: int, current_state, players) -> void:
	for player_data in players:
		var peer_id: int = player_data[0]

		# from data
		var _time: float = player_data[1]  # for now we're not using this, maybe in the future for tickless? lmao.
		var movement_input: Vector3 = player_data[2]
		var actions: Array = player_data[3]
		var parent_pivot_transform: Transform3D = player_data[4]
		var pivot_transform: Transform3D = player_data[5]
		var current_weapon_slot: int = player_data[6]
		var client_trust_data: Dictionary = player_data[7]
		var shots_to_be_consumed: Array = player_data[8]

		var peer_info = current_state.get(peer_id)
		if peer_info:
			var player = current_state[peer_id]["player"]
			if player != null:
				# BUG: commented for now because it doesn't do anything lmao
				# only populate yourself for client prediction (to make things smooth)
				# if player.is_owned_by_me():
				# 	var snapshot_data = DEFAULT_PLAYER_SNAPSHOT.duplicate(true)
				# 	snapshot_data["global_transform"] = Transform3D(player.global_transform)
				# 	snapshot_data["velocity"] = Vector3(player.velocity)
				# 	self.populate_timeline_snapshot(Engine.get_physics_frames(), peer_id, snapshot_data)
				if not player.is_node_ready():
					continue

				player.set_multiplayer_pull(client_tick_to_compare, movement_input, actions, parent_pivot_transform, pivot_transform, current_weapon_slot, client_trust_data, shots_to_be_consumed)

		if not peer_info:
			gmd_add_player(Transform3D.IDENTITY, peer_id, 0)


func build_client_trust_data(transform: Transform3D, velocity: Vector3, health: float, armor: float, client_shots_to_be_sent: Array) -> Dictionary:
	return {
		"transform": transform,
		"velocity": velocity,
		"health": health,
		"armor": armor,
		"client_shots_to_be_sent": client_shots_to_be_sent,
	}


# will return with -1 if physics_frame_count is invalid
# WARNING: if invalid, the caller should instead sync everything based on the latest snapshot available
func build_timeline_snapshot_index(physics_frame_count) -> int:
	if physics_frame_count > Engine.get_physics_frames():
		return -1

	var oldest_physics_frame_in_buffer = max(Engine.get_physics_frames() - self.player_snapshot_timeline.size() + 1, 0)
	if oldest_physics_frame_in_buffer > physics_frame_count:
		return -1

	return physics_frame_count % self.player_snapshot_timeline.size()


# will return empty dictionary when physics_frame_count is invalid
func get_timeline_snapshot(physics_frame_count: int, tag: String) -> Dictionary:
	var index = self.build_timeline_snapshot_index(physics_frame_count)
	if index < 0:
		return {}

	return self.player_snapshot_timeline[index][tag]

"""
the snapshot timeline is used client and server side with several differences.
SERVER-SIDE:
	- all players' state are snapshoted per frame
	- rollback are used for shooting mechanic for example, where client might see an enemy several frames before the current server frame.
CLIENT-SIDE:
	- only snapshot the currently owned player
	- used for client side prediction, movement is done instantly on the client, but will be snapped back if it deviates too much
	- used for reconciliation too

@param unique_key for "players" it's their peer_id for "items" its their unique id.
@param tag currently only filled with "players" and "items".
@param snapshot_data this variable will contain many different thing. Player and items have different structures for example.
"""
func populate_timeline_snapshot(physics_frame_count: int, tag: String, unique_key, snapshot_data: Dictionary) -> void:
	var index = physics_frame_count % self.player_snapshot_timeline.size()
	self.player_snapshot_timeline[index][tag][unique_key] = snapshot_data


###########################################################
# GAME MODE GENERAL
###########################################################

func handle_added_player(player: Node):
	# Connect i health when dead and add the player themselves as binding value
	if Lobby.is_authoritative() and "i_health" in player:
		PlayerStats.register_player(player.owned_by_peer_id)
		player.i_health.dead.connect(Callable(self, "_on_player_ihealth_dead").bind(player))
		player.i_health.dead.connect(PlayerStats.register_death.bind(player.owned_by_peer_id))
		player.i_health.damage_done.connect(PlayerStats.register_damage_done.bind(player.owned_by_peer_id))


func handle_new_level(level: Node):
	var game_mode = State.get_state("game_mode")

	match game_mode:
		Global.GAME_MODE.DEFAULT:
			gmd_handle_new_level(level)
		Global.GAME_MODE.AIM_TRAINING, Global.GAME_MODE.CUSTOM:
			print(Global.LOG_INFO, "This game mode is handled by the level script not by game_mode.gd")
		_:
			print_stack()
			printerr("Game Mode Doesn't Exist")


###########################################################
# GAME MODE SECTION
#
# gmd  => Game Mode DEFAULT
# glc  => Game Mode LONG_COMPETITIVE
# gsc  => Game Mode SHORT_COMPETITIVE
###########################################################

# all function inside will assume the game mode is default
func gmd_physics_process(delta: float) -> void:
	gmd_handle_player_spawn_from_queue()
	gmd_timer_handler()


func gmd_timer_handler() -> void:
	if not Lobby.is_authoritative():
		return

	var current_state = State.get_state("game_mode_state")
	var current_time = Time.get_unix_time_from_system()

	match current_state.state:
		Global.DEFAULT_GAME_MODE_STATE.STARTING:
			# if the time ends, then change to ongoing state
			if current_state.gmd.state_starting_end_unix < current_time:
				var new_state = current_state.duplicate(true)
				new_state.state = Global.DEFAULT_GAME_MODE_STATE.ONGOING
				self.gmd_change_state.rpc(new_state, "")

		Global.DEFAULT_GAME_MODE_STATE.ONGOING:
			# if the time ends, then change to ENDING state
			if current_state.gmd.state_ongoing_end_unix < current_time:
				var new_state = current_state.duplicate(true)
				new_state.state = Global.DEFAULT_GAME_MODE_STATE.ENDING
				self.gmd_change_state.rpc(new_state, "")

				var default_camera = get_tree().get_nodes_in_group(Global.GROUP_DEFAULT_LEVEL_CAMERA).front()
				default_camera.make_current()

				# remove all players
				var peer_ids = State.get_state("multiplayer_player_info").keys()
				self.gmd_remove_player.rpc(peer_ids)

				# TODO-DIHAR: remove all items, smokes, and decals

		Global.DEFAULT_GAME_MODE_STATE.ENDING:
			# if the time ends, then change to starting state
			if current_state.gmd.state_ending_end_unix < current_time:
				var new_state = GameMode.gmd_initial_state_builder()
				self.gmd_change_state.rpc(new_state, "")


func gmd_handle_new_level(_level):
	# for now, nothing.
	pass


# GAME_MODE will always be assumed to be Global.GAME_MODE.DEFAULT
@rpc("authority", "call_local", "reliable")
func gmd_change_state(new_state: Dictionary, level_path: String) -> void:
	var old_state = State.get_state("game_mode_state")

	match old_state.state:
		Global.DEFAULT_GAME_MODE_STATE.LOBBY:
			match new_state.state:
				Global.DEFAULT_GAME_MODE_STATE.STARTING, Global.DEFAULT_GAME_MODE_STATE.ONGOING, Global.DEFAULT_GAME_MODE_STATE.ENDING:
					if multiplayer.is_server():
						Lobby.load_map.rpc(level_path, Global.GAME_MODE.DEFAULT, new_state)
					if not multiplayer.is_server():
						Lobby.load_map(level_path, Global.GAME_MODE.DEFAULT, new_state)

	State.set_state("game_mode_state", new_state)


func gmd_handle_player_spawn_from_queue() -> void:
	if Lobby.is_authoritative():
		var queue = State.get_state("multiplayer_spawn_queue")
		if queue.is_empty():
			return

		# Add To World3D
		# Consequently, will be handled by handle_added_player automagically in _on_tree_node_added
		var universal_spawn_points = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL)
		var team_1_spawn_points = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_TEAM_1)
		var team_2_spawn_points = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_TEAM_2)

		# Universal, means everyone
		universal_spawn_points.append_array(team_1_spawn_points)
		universal_spawn_points.append_array(team_2_spawn_points)

		if universal_spawn_points.is_empty():
			printerr("map has no spawn point with group either: SPAWN_TYPE_GROUP_UNIVERSAL, SPAWN_TYPE_GROUP_TEAM_1, SPAWN_TYPE_GROUP_TEAM_2")
			return

		var peers = PackedInt32Array([0])

		if Lobby.is_lobby_master():
			peers = multiplayer.get_peers()
			peers.append(multiplayer.get_unique_id())

		for q in queue:
			var global_spawn_point = universal_spawn_points.pick_random()

			match q.team_enum:
				Global.TEAM.TEAM_1:
					if not team_1_spawn_points.is_empty():
						global_spawn_point = team_1_spawn_points.pick_random()
				Global.TEAM.TEAM_2:
					if not team_2_spawn_points.is_empty():
						global_spawn_point = team_2_spawn_points.pick_random()

			self.gmd_add_player.rpc(global_spawn_point.global_transform, q.peer_id, q.team_enum)

		State.set_state("multiplayer_spawn_queue", [])


@rpc("authority", "call_local", "reliable")
func gmd_add_player(global_spawn_point: Transform3D, peer_id: int, team_enum: int) -> void:
	# If exists then free it
	var player_info = State.get_state("multiplayer_player_info")
	if peer_id in player_info:
		var current_info = player_info[peer_id]
		if is_instance_valid(current_info.player):
			var dropped_weapons = current_info.player.drop_all_weapon(false)
			var current_item_state = State.get_state("multiplayer_item_alive").duplicate(true)

			for w in dropped_weapons:
				current_item_state.erase(w.weapon_id)
				w.queue_free()

			State.set_state("multiplayer_item_alive", current_item_state)
			current_info.player.queue_free()

	# Add To World3D
	# Consequently, will be handled by handle_added_player automagically in _on_tree_node_added
	var new_player = player_scene.instantiate()
	new_player.global_transform = global_spawn_point
	new_player.owned_by_peer_id = peer_id
	new_player.team_enum = team_enum

	var current_state = State.get_state("multiplayer_player_info").duplicate(true)  # TODO: can we actually just duplicate a reference to a node?
	current_state[peer_id] = {
		"player": new_player
	}
	State.set_state("multiplayer_player_info", current_state)
	Util.add_to_world(new_player)

	if not new_player.is_node_ready():
		await new_player.ready

	# give them default weapon
	if Lobby.is_authoritative():
		self.add_weapon_to_player.rpc(peer_id, "pm9", Util.random_string_generator())


func gmd_handle_player_dead(player):
	if Lobby.is_authoritative():
		var team_enum = player.team_enum
		var peer_id = player.owned_by_peer_id

		# kill before trying to respawn
		# TODO: create self.kill_player.rpc(peer_id)
		player.queue_free()

		var global_spawn_point = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL).pick_random().global_transform
		var new_spawn_queue = []
		new_spawn_queue.append_array(State.get_state("multiplayer_spawn_queue"))
		new_spawn_queue.append({
			"spawn_global_transform": global_spawn_point,
			"peer_id": peer_id,
			"team_enum": team_enum,
		})

		State.set_state("multiplayer_spawn_queue", new_spawn_queue)


# This will remove the player without triggering the health
@rpc("authority", "call_local", "reliable")
func gmd_remove_player(peer_ids: Array) -> void:
	var player_info = State.get_state("multiplayer_player_info")
	for peer_id in peer_ids:
		if peer_id > -1:
			var player_state = player_info[peer_id]
			if player_state == null:
				return

			var player = player_state["player"]
			if player == null or not is_instance_valid(player):
				return

			player.queue_free()
			player_info.erase(peer_id)

	State.set_state("multiplayer_player_info", player_info)


func gmd_initial_state_builder() -> Dictionary:
	var current_time = Time.get_unix_time_from_system() # in second

	# TODO: for now all of the time are hardcoded, should be able to config by room master
	var starting_end = current_time + 5 # 10 seconds of waiting
	var ongoing_end = starting_end + 60 * 10 # 10 minutes of play
	var ending_end = ongoing_end + 5 # 10 seconds of end screen

	return {
		"state": Global.DEFAULT_GAME_MODE_STATE.STARTING,
		"gmd": {
			"state_starting_end_unix": starting_end,
			"state_ongoing_end_unix": ongoing_end,
			"state_ending_end_unix": ending_end,
		}
	}


@rpc("any_peer", "call_local", "reliable")
func gmd_choose_team(team_enum: Global.TEAM) -> void:
	if not multiplayer.is_server():
		return

	var global_spawn_point = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL).pick_random().global_transform
	var new_spawn_queue = []

	var peer_id = multiplayer.get_remote_sender_id()

	# TODO: maybe kill the player before spawning them? Useful for when changing team mid round

	new_spawn_queue.append_array(State.get_state("multiplayer_spawn_queue"))
	new_spawn_queue.append({
		"spawn_global_transform": global_spawn_point,
		"peer_id": peer_id,
		"team_enum": team_enum,
	})

	State.set_state("multiplayer_spawn_queue", new_spawn_queue)
	Lobby.set_lobby_information.rpc(Lobby.set_player_team(peer_id, team_enum))


@rpc("any_peer", "call_local", "reliable")
func gmd_buy_weapon(weapon_id: String) -> void:
	if not multiplayer.is_server():
		return

	var game_mode_enum = State.get_state("game_mode")
	var game_mode_state_info = State.get_state("game_mode_state")

	if game_mode_enum == Global.GAME_MODE.DEFAULT and game_mode_state_info.state != Global.DEFAULT_GAME_MODE_STATE.ONGOING:
		return

	if not WEAPON_REGISTRY.has(weapon_id):
		return

	# game mode default doesn't care about money, you can buy anything you want
	var peer_id = multiplayer.get_remote_sender_id()
	self.add_weapon_to_player.rpc(peer_id, weapon_id, Util.random_string_generator())  # TODO: ONLY FOR TESTING, remove this weapon when buying weapon is possible


###########################################################
# OTHER ACTIONS
###########################################################

# TODO: need check, what happened if player already have gun in the same slot? it will be dropped right? does that get synced betweeen server - client?
# weapon_id is the key you get from WEAPON_REGISTRY
@rpc("authority", "call_local", "reliable")
func add_weapon_to_player(peer_id: int, weapon_name: String, weapon_id: String) -> void:
	var current_item_state = State.get_state("multiplayer_item_alive").duplicate(true)
	if current_item_state.get(weapon_id):
		return

	var weapon_resource = WEAPON_REGISTRY[weapon_name]
	if weapon_resource == null:
		return

	var weapon_instance = weapon_resource.instantiate()
	weapon_instance.weapon_id = weapon_id

	# add a weapon id so server and client can track it together
	current_item_state[weapon_id] = weapon_instance
	State.set_state("multiplayer_item_alive", current_item_state)

	# ONLY IF PEER_ID EXIST: give weapon only once the instance is ready
	if peer_id > -1:
		var player_state = State.get_state("multiplayer_player_info")[peer_id]
		if player_state == null:
			return

		var player = player_state["player"]
		if player == null or not is_instance_valid(player):
			return

		weapon_instance.ready.connect(give_weapon_to_player_after_weapon_ready.bind(player, weapon_instance))

	# add to world ONLY at the end
	Util.add_to_world(weapon_instance)


func give_weapon_to_player_after_weapon_ready(player: Node, weapon_instance: Node) -> void:
	player.weapon_pickup_routine(weapon_instance)

###########################################################
# SIGNAL HANLDER
###########################################################

func _on_tree_node_added(node: Node) -> void:
	if Util.is_in_either_group(node, [Global.GROUP_PLAYER]):
		await node.ready  # Node must be ready before processing
		handle_added_player(node)
	if node.is_in_group(Global.GROUP_LEVEL):
		await node.ready
		handle_new_level(node)


func _on_player_ihealth_dead(player):
	var game_mode = State.get_state("game_mode")

	match game_mode:
		Global.GAME_MODE.DEFAULT:
			gmd_handle_player_dead(player)
		_:
			print_stack()
			printerr("Game Mode Doesn't Exist")
