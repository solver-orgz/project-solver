extends Node


###########################################################
# GLOBALLY USED FUNCTIONS
# Any function that need to be applied globally
###########################################################

func _ready():
	Util.handle_err(get_tree().connect("node_added", Callable(self, "_on_tree_node_added")))


func _on_tree_node_added(node: Node) -> void:
	if node is Light3D:
		make_light_available_every_layer(node)


func make_light_available_every_layer(node: Light3D) -> void:
	node.layers = MAX_INT


###########################################################
# GLOBALLY USED CONSTANTS
# Anything that's copy by value and could be a constant
###########################################################

# Game versioning must abide with https://semver.org/, especially https://semver.org/#backusnaur-form-grammar-for-valid-semver-versions
# As a general guidance:
#   1. Bug fixes or small/medium features must bump patch version. Categorization of how big a feature is at the discretion of developers.
#   2. New notable features must bump minor version. Categorization of how big a feature is at the discretion of developers.
#   3. Changes that will break the compatibility of configuration file, training history, and API, must bump major version.
#   4. Developers can add pre-release identifier such as "-alpha" or "-beta" to communicate the state of the program better to users.
const GAME_VERSION = "0.8.0-alpha"

const PHI = 1.61803399
const MATERIAL_GROUP_PREFIX = "MATERIAL__"
const MAX_INT = 9223372036854775807

const LOG_INFO = "[INFO] "
const LOG_DEBUG = "[DEBUG] "


###########################################################
# GLOBALLY USED ENUM + STRINGS
# Basically enums, but with values other than an integer
###########################################################

const GROUP_LEVEL = "LEVEL"
const GROUP_DECAL_BULLET = "DECAL_BULLET"
const GROUP_ANTI_DECAL_BULLET = "ANTI_DECAL_BULLET"  # object that's in this group will not have bullet decal on their bodies
const GROUP_PLAYER = "PLAYER"
const GROUP_DEFAULT_LEVEL_CAMERA = "DEFAULT_LEVEL_CAMERA"


const SPAWN_TYPE_GROUP_UNIVERSAL = "SPAWN_TYPE_GROUP__UNIVERSAL"
const SPAWN_TYPE_GROUP_TEAM_1 = "SPAWN_TYPE_GROUP__TEAM_1"
const SPAWN_TYPE_GROUP_TEAM_2 = "SPAWN_TYPE_GROUP__TEAM_2"


# The values inside of MATERIAL should be added to physical bodies

const MATERIAL_GENERIC = "MATERIAL__GENERIC"
const MATERIAL_WOOD = "MATERIAL__WOOD"
const MATERIAL_GLASS = "MATERIAL__GLASS"

# FOV value

const FOV_DEFAULT = 70.0
const FOV_SNIPER_ZOOMED_1 = 40.0
const FOV_SNIPER_ZOOMED_2 = 10.0

###########################################################
# GLOBALLY USED ENUMS
# Enums that are used... Globally...
###########################################################

enum GAME_MODE {
	# The player will be spawned in immediately to a universal spawn point
	# and will be respawned in that spawn point when they die.
	DEFAULT = 1,

	# A competitive mode that's taking the default amount of rounds to win
	LONG_COMPETITIVE,

	# A competitive mode that's taking a shorter amount of rounds to win
	SHORT_COMPETITIVE,

	# Training mode in which the player cannot walk and given gun with infinite ammo
	# Same as CUSTOM, Player spawning and other routines will not be handled by `autoload/game_mode.gd` but by the level script itself.
	AIM_TRAINING,

	# Player spawning and other routines will not be handled by `autoload/game_mode.gd` but by the level script itself.
	CUSTOM
}

enum DEFAULT_GAME_MODE_STATE {
	LOBBY = 0,
	STARTING = 1,
	ONGOING = 2,
	ENDING = 3,
}

enum WEAPON_TYPE {
	AUTOMATIC,
	SEMI_AUTOMATIC,
	SNIPER_SEMI_AUTOMATIC,
	KNIFE,
	FRAG_GRENADE,
	FLASH_GRENADE,
	SMOKE_GRENADE,
	PROGRAMMED_TRIGGERED
}

enum WEAPON_SLOT {
	PRIMARY,
	SECONDARY,
	MELEE,
	UTILITY
}

enum RENDER_LAYERS {
	WORLD = 1,
	PLAYER = 2
}

enum TEAM {
	DEFAULT,
	TEAM_1,
	TEAM_2,
}

enum PHYSICS_LAYERS {
	WORLD = 1,
	TEAM_1 = 2,
	TEAM_2 = 4,
	GUN = 8,
	MISC_CLIP = 16  # For physics object that's supposed to influence others but are not player related.
}

enum WEAPON_ZOOM_MODE {
	DEFAULT,
	SNIPER_ZOOMED_1,
	SNIPER_ZOOMED_2
}

enum SHOOTING_TARGET_MOVEMENT_MODE {
	STATIC,
	MOVING,
}

enum AMMO_INFINITE_MODE {
	NONE,  # Normal mode where ammo and magazine is spent, basically no infinite mode.
	INFINITE_AMMO,  # Infinite ammo where the player doesn't need to reload.
	INFINITE_MAGAZINE  # Infinite magazine where the player need to reload from a pool of infinite magazine.
}

# WARNING: changing weapon should be dictated by the slots instead
enum PLAYER_INPUT_ACTIONS {
	INTERACT = 1,
	WALK = 2,
	JUMP = 3,
	JUMP_JUST_FRAME = 4,
	CROUCH = 5,
	SHOOT_1_PRESSED = 6,
	SHOOT_1_RELEASED = 7,
	SHOOT_2_PRESSED = 8,
	SHOOT_2_RELEASED = 9,
	RELOAD = 10,
	DROP_WEAPON = 11,
}

# used to indicate whether that particular process have/haven't consumed a value yet
enum PROCESS_CONSUMER {
	PROCESS = 1,
	PHYSICS_PROCESS = 2,
}

###########################################################
# GLOBALLY USED FUNCTIONS
# Most of the time to map enum with their respective values
###########################################################

func get_material_penetration_coefficient(node: Node) -> float:
	var value = get_material_groups(node)
	return match_material_penetration_coeficient(value)


func get_material_groups(node: Node):
	var result = Util.get_groups_with_prefix(node, MATERIAL_GROUP_PREFIX)

	# HACK: I'm using this ugly code because the engine will get mad if I use super.front() checked an empty array
	if result.size() > 0:
		return result.front()
	else:
		return null


func match_material_penetration_coeficient(surface_id) -> float:
	match surface_id:
		MATERIAL_GENERIC:
			return 75.0
		MATERIAL_WOOD:
			return 25.0
		MATERIAL_GLASS:
			return 10.0
		_:
			return 75.0
