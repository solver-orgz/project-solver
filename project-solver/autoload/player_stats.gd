extends Node

var _stats_state = {
	"gmd": {
		# defined in insert_gmd_damage_cache
		"damage_cache": {},

		# will store player stats such as the total damage incurred
		# format in DEFAULT_PLAYER_STATS
		"player_stats": {},
	}
}

const MINIMAL_DAMAGE_BEFORE_ASSIST = 30.0 # only register an assist if the player at least does 30 damage
const DEFAULT_PLAYER_STATS = {
	"kill": 0,
	"death": 0,
	"assist": 0,
	"damage": 0.0,
}

signal shot_fired(player, hit_list) # instance of player.gd and spatial node that was hit

func emit_shot_fired(player: Node3D, hit_list: Array) -> void:
	emit_signal("shot_fired", player, hit_list)

func insert_gmd_damage_cache(sender: int, receiver: int, damage: float) -> void:
	var timestamp: float = Time.get_unix_time_from_system()
	var key: String = "%d-%d" % [sender, receiver]

	if self._stats_state.gmd.damage_cache.has(key):
		self._stats_state.gmd.damage_cache[key].damage += damage
		self._stats_state.gmd.damage_cache[key].timestamp += timestamp 

	if not self._stats_state.gmd.damage_cache.has(key):
		self._stats_state.gmd.damage_cache[key] = {
			"sender": sender,
			"receiver": receiver,
			"damage": damage,
			"timestamp": timestamp,
		}

func add_gmd_damage(sender: int, damage: float) -> void:
	if not self._stats_state.gmd.player_stats.has(sender):
		self._stats_state.gmd.player_stats[sender] = DEFAULT_PLAYER_STATS.duplicate(true)

	self._stats_state.gmd.player_stats[sender].damage = self._stats_state.gmd.player_stats[sender].damage + damage

# will be called upon the receiver peer_id death
func clear_gmd_damage_cache_by_receiver(receiver_id: int) -> void:
	# 1. get all dict keys with receiver_id
	var keylist: Array[String] = []
	var receiver_substring_key = self.build_receiver_substring_key(receiver_id)
	for k in self._stats_state.gmd.damage_cache.keys():
		if receiver_substring_key in k:
			keylist.append(k)

	# 2. erase it from state
	for k in keylist:
		self._stats_state.gmd.damage_cache.erase(k)


func build_receiver_substring_key(receiver_id: int) -> String:
	return "-%d" % receiver_id


func get_gmd_damage_cache_by_receiver(receiver_id: int) -> Array[Dictionary]:
	var receiver_stats: Array[Dictionary] = []
	var receiver_key_substring_key = self.build_receiver_substring_key(receiver_id)
	for k in self._stats_state.gmd.damage_cache:
		if receiver_key_substring_key in k:
			receiver_stats.append(self._stats_state.gmd.damage_cache[k])

	return receiver_stats


func register_player(peer_id: int) -> void:
	if not self._stats_state.gmd.player_stats.has(peer_id):
		self._stats_state.gmd.player_stats[peer_id] = DEFAULT_PLAYER_STATS.duplicate(true)


# sender and receiver is player peer id
func register_damage_done(sender: int, damage: float, receiver: int,) -> void:
	insert_gmd_damage_cache(sender, receiver, damage)
	add_gmd_damage(sender, damage)


func register_death(recently_dead_peer_id: int) -> void:
	# 1. register to death
	self._stats_state.gmd.player_stats[recently_dead_peer_id].death += 1 

	# 2. register to kill and assist
	# get all damages dealt to recently_dead_peer_id
	var damage_caches = self.get_gmd_damage_cache_by_receiver(recently_dead_peer_id)
	damage_caches.sort_custom(func(a, b): return a.timestamp > b.timestamp)

	var killer_peer_id = -1000
	var assist_peer_id = -1000

	# 3. assign killer and assist peer id
	if len(damage_caches) > 0:
		killer_peer_id = damage_caches[0].sender

	# 3.1 take the highest damaging player other than the killer and assign them as assist, if damage more than MINIMAL_DAMAGE_BEFORE_ASSIST
	if len(damage_caches) > 1:
		var damage_caches_sorted_by_damage = damage_caches.slice(1)
		damage_caches_sorted_by_damage.sort_custom(func(a, b): return a.damage > b.damage)
		
		if -damage_caches_sorted_by_damage[0].damage >= MINIMAL_DAMAGE_BEFORE_ASSIST:
			assist_peer_id = damage_caches_sorted_by_damage[0].sender

	# 4. add data to gmd player_stats
	if killer_peer_id != -1000:
		self._stats_state.gmd.player_stats[killer_peer_id].kill += 1
	
	if assist_peer_id != -1000:
		self._stats_state.gmd.player_stats[assist_peer_id].assist += 1

	# 5. sync data to multiplayer_lobby_player_info state
	self.sync_gmd_player_stats_to_lobby_info()

	# 6. clear damage cache of dead guy
	self.clear_gmd_damage_cache_by_receiver(recently_dead_peer_id)

	# 7. sync data to other peers
	Lobby.server_sync_lobby_info_to_peers()


func sync_gmd_player_stats_to_lobby_info() -> void:
	for k in self._stats_state.gmd.player_stats.keys():
		var value = self._stats_state.gmd.player_stats[k]
		var info = State.get_state("multiplayer_lobby_player_info")

		Lobby.set_lobby_info_stats(k, value.kill, value.death, value.assist, value.damage, info[k].team_enum)
