extends Node

signal shot_fired(player, hit_list) # instance of player.gd and spatial node that was hit

func emit_shot_fired(player: Node3D, hit_list: Array) -> void:
    emit_signal("shot_fired", player, hit_list)