extends HBoxContainer

func set_texts(is_current_player: bool, peer_id, player_name, kill, death, assist, damage, ping) -> void:
	$PeerIDLabel.text = str(peer_id)
	$PlayerNameLabel.text = str(player_name)
	$KDALabel.text = "%s / %s / %s" % [str(kill), str(death), str(assist)]
	$DamageLabel.text = str(damage)
	$PingLabel.text = str(ping)

	if is_current_player:
		$PlayerNameLabel.text = "(you) " + str(player_name)