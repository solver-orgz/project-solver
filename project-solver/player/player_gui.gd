extends Control

@onready var crosshair = $CrosshairProcedural
@onready var gun_container = $GunContainer
@onready var sniper_scope = $SniperScope
@onready var sniper_dot = $SniperCrosshairProcedural
@onready var weapon_name = $Indicators/AmmoIndicator/HBoxContainer/VBoxContainer2/WeaponName
@onready var current_ammo = $Indicators/AmmoIndicator/HBoxContainer/VBoxContainer2/HBoxContainer/CurrentAmmo
@onready var total_ammo = $Indicators/AmmoIndicator/HBoxContainer/VBoxContainer2/HBoxContainer/TotalAmmo
@onready var health_bar = $Indicators/AmmoIndicator/HBoxContainer/VBoxContainer/HealthBar
@onready var armor_bar = $Indicators/AmmoIndicator/HBoxContainer/VBoxContainer/ArmorBar
@onready var quick_menu = $QuickMenu


func _ready():
	Util.handle_err(State.connect("state_player_zoom_mode", Callable(self, "_on_state_player_zoom_made")))
	Util.handle_err(State.connect("state_player_weapon_name", Callable(self, "_on_state_player_weapon_name")))
	Util.handle_err(State.connect("state_player_weapon_current_ammo", Callable(self, "_on_state_player_weapon_current_ammo")))
	Util.handle_err(State.connect("state_player_weapon_total_ammo", Callable(self, "_on_state_player_weapon_total_ammo")))
	Util.handle_err(State.connect("state_player_health", Callable(self, "_on_state_player_health")))
	Util.handle_err(State.connect("state_player_armor", Callable(self, "_on_state_player_armor")))
	_on_state_player_zoom_made(State.get_zoom_level(self.multiplayer.get_unique_id()))
	if (Config.state.game.god_mode_enabled == true):
		$GodModeCanvasLayer.show()

	quick_menu.hide()


func _physics_process(_delta):
	handle_sniper_dot_visiblity()
	handle_quick_menu()


func _on_state_player_zoom_made(_value):
	var zoom_level = State.get_zoom_level(self.multiplayer.get_unique_id())

	sniper_scope.visible = is_player_zoom_visible(zoom_level)
	crosshair.visible = !is_player_zoom_visible(zoom_level)
	gun_container.visible = !is_player_zoom_visible(zoom_level)


func is_player_zoom_visible(zoom_state) -> bool:
	return zoom_state > Global.WEAPON_ZOOM_MODE.DEFAULT


func handle_sniper_dot_visiblity() -> void:
	var zoom_is_visible = is_player_zoom_visible(State.get_zoom_level(self.multiplayer.get_unique_id()))
	zoom_is_visible = zoom_is_visible && State.get_state("player_velocity_length") == 0.0

	sniper_dot.visible = zoom_is_visible


func handle_quick_menu() -> void:
	# BUG: because tab is already done here in ui_focus_next
	if Input.is_action_pressed("ui_focus_next"):
		quick_menu.show()

	# BUG: because tab is already done here in ui_focus_next
	if not Input.is_action_pressed("ui_focus_next"):
		quick_menu.hide()


func _on_state_player_weapon_name(value) -> void:
	weapon_name.text = str(value)


func _on_state_player_weapon_current_ammo(value) -> void:
	current_ammo.text = str(value)


func _on_state_player_weapon_total_ammo(value) -> void:
	total_ammo.text = str(value)


func _on_state_player_health(value) -> void:
	health_bar.value = value


func _on_state_player_armor(value) -> void:
	armor_bar.value = value
