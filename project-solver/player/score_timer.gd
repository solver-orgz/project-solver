extends Label

func _ready():
	if not (State.get_state("game_mode") == Global.GAME_MODE.AIM_TRAINING and Score.mode == Score.Mode.RANDOM_SINGLE_TIMED):
		hide()


func _physics_process(_delta):
	if Score.rst_physics_tick_start > 0:
		var current_tick = Engine.get_physics_frames()
		var elapsed_tick = current_tick - Score.rst_physics_tick_start
		var second_per_tick = 1.0 / Engine.physics_ticks_per_second
		var elapsed_seconds = elapsed_tick * second_per_tick

		var minute = floor(elapsed_seconds / 60)
		self.text = "%02d:%02.3f" % [minute, fmod(elapsed_seconds, 60.0)]
	else:
		self.text = "FINISHED"
