// Originally https://github.com/0xspig/CrosshairShader, Licensed under MIT
// Modified by Tengku Izdihar, Licensed under AGPLv3

shader_type canvas_item;

uniform bool center_enabled = true;
uniform bool legs_enabled = true;
uniform bool inverted = false;
uniform vec4 color_0: source_color = vec4(0., 1, 0., 1.);
uniform float center_radius = .002;
uniform float width = .001;
uniform float len = .009;
uniform float spacing = .006;
uniform float spread = 1.;
uniform float leg_alpha = 1.;
uniform float top_leg_alpha = 1.;
uniform sampler2D OLD_SCREEN_TEXTURE : hint_screen_texture, filter_linear_mipmap;


void fragment(){
	float a = SCREEN_PIXEL_SIZE.x / SCREEN_PIXEL_SIZE.y;
	vec2 UVa = vec2(UV.x, UV.y);
	vec2 center = vec2(.5, .5);

	float point = step(distance(UVa, center), center_radius);

	float h = step(center.x - len - spacing*spread, UVa.x) - step(center.x - spacing*spread, UVa.x);
	h += step(center.x + spacing*spread, UVa.x) - step(center.x + len + spacing*spread, UVa.x);
	h *= step(center.y - width, UVa.y) - step(center.y + width, UVa.y);

	float v = (step(center.y - len - spacing*spread, UVa.y) - step(center.y - spacing*spread, UVa.y)) * top_leg_alpha;
	v += step(center.y + spacing*spread, UVa.y) - step(center.y + len + spacing*spread, UVa.y);
	v *= step(center.x - width, UVa.x) - step(center.x + width, UVa.x);

	float crosshair;

	crosshair = (h+v) * float(legs_enabled) * leg_alpha + point * float(center_enabled);

	if(!inverted){
		COLOR = color_0 * crosshair;
	}else{
		COLOR = vec4((cos(textureLod(OLD_SCREEN_TEXTURE, SCREEN_UV, 0.0).rgb * 3.1415926534) + 1.)/2., 1.) * crosshair;
	}
}