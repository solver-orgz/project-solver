extends Control

@onready var time_left_label = $VBoxContainer/TimeLeftLabel

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	set_time_left_label()


func set_time_left_label() -> void:
	var state = State.get_state("game_mode_state")
	var current_time = Time.get_unix_time_from_system()
	var time_left = max(state.gmd.state_ending_end_unix - current_time, 0.0)

	time_left_label.text = "%*.*f" % [7, 1, time_left]
