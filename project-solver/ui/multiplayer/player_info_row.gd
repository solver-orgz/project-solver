extends PanelContainer

var player_network_id = 0

@onready var player_name_label = self.get_node("%PlayerName")
@onready var kick_button = self.get_node("%KickButton")


func set_player_info(player_name: String, network_id: int, kick_disable: bool) -> void:
	self.player_name_label.text = player_name
	self.player_network_id = network_id
	self.kick_button.disabled = kick_disable


func _on_kick_button_pressed():
	Lobby.kick_player_from_lobby(self.player_network_id)
