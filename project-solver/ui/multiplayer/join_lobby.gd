extends CenterContainer


func _on_button_pressed():
	var nametag_value = self.get_node("%NameTagEdit").get_text()
	var ip_address_value = self.get_node("%IPAddressEdit").get_text()
	var port_value = self.get_node("%PortEdit").get_text()
	var password_value = self.get_node("%PasswordEdit").get_text()

	Lobby.connect_and_enter_lobby(nametag_value, ip_address_value, port_value.to_int(), password_value)
