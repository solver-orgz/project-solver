extends PanelContainer

@onready var level_path = ""
@onready var level_name = ""
@onready var is_current = false
@onready var label = self.get_node("%Label")


func _ready():
	State.connect("state_multiplayer_lobby_info", self._on_State_multiplayer_lobby_info)


func _physics_process(_delta):
	var current_text = ""
	if is_current:
		current_text = "(current) "

	label.text = "%s%s" % [current_text, level_name]


func _on_SelectButton_pressed():
	Lobby.lobby_master_change_level_path(level_path)


func _on_State_multiplayer_lobby_info(lobby_info: Dictionary) -> void:
	is_current = lobby_info.level_path == level_path
	
