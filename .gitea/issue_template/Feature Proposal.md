---
name: Feature Proposal
about: Create a feature proposal to solve a problem
---

## Please Confirm The Following (required):
- [x] Check the boxes as such.
- [ ] I have searched the [issues](https://codeberg.org/solver-orgz/project-solver/issues), and I didn't find a proposal similar to mine.
- [ ] If you upload an image or other content, please make sure you have read and understood the [Codeberg Terms of Use](https://codeberg.org/Codeberg/org/src/branch/main/TermsOfUse.md)

## Your New Feature Is (required):

<!-- Please describe your new feature in concise and informative manner -->

## Will It Break Compatibility (required):

<!-- Please describe what already implemented features might be affected by your proposal -->