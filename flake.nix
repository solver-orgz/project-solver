{
  # main
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/1925c603f17fc89f4c8f6bf6f631a802ad85d784";
  };

  # dev
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        # godot = pkgs.callPackage ./project-solver/nix/godot4.nix {};
      in
      {
        devShells.default = pkgs.mkShell {
          packages =
            [
              pkgs.godot_4
              pkgs.just
            ];
        };
      }
    );
}
